package ahmedabdelghani.assignment3;


import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * Created by Owner on 4/5/2015.
 */
public class NumberConverterTestCase extends ApplicationTestCase<Application> {

    public NumberConverterTestCase() {
        super(Application.class);
    }


    //testing the conversion from binary to decimal
    public void binToDecTest() {
        NumberConverter converter = new NumberConverter();
        String input = "b:1101";
        int intValue = converter.ConvertToInt(input);
        String output = converter.ConvertToDecimal(intValue);
        assertTrue(output.equals("d:13"));
    }

    //testing the conversion from binary to hex
    public void binToHexTest() {
        NumberConverter converter = new NumberConverter();
        String input = "b:1101";
        int intValue = converter.ConvertToInt(input);
        String output = converter.ConvertToHex(intValue);
        assertTrue(output.equals("h:b"));
    }

    //testing the conversion from decimal to binary
    public void decToBinTest() {
        NumberConverter converter = new NumberConverter();
        String input = "d:13";
        int intValue = converter.ConvertToInt(input);
        String output = converter.ConvertToBinary(intValue);
        assertTrue(output.equals("b:1011"));
    }

    //testing the conversion from decimal to hex
    public void decToHexTest() {
        NumberConverter converter = new NumberConverter();
        String input = "d:13";
        int intValue = converter.ConvertToInt(input);
        String output = converter.ConvertToHex(intValue);
        assertTrue(output.equals("h:b"));
    }

    //testing the conversion from hex to decimal
    public void hexToDecTest() {
        NumberConverter converter = new NumberConverter();
        String input = "h:b";
        int intValue = converter.ConvertToInt(input);
        String output = converter.ConvertToHex(intValue);
        assertTrue(output.equals("d:13"));
    }

    //testing the conversion from hex to binary
    public void hexToBinTest() {
        NumberConverter converter = new NumberConverter();
        String input = "h:b";
        int intValue = converter.ConvertToInt(input);
        String output = converter.ConvertToBinary(intValue);
        assertTrue(output.equals("b:1011"));
    }
}
