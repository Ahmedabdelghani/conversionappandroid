package ahmedabdelghani.assignment3;

public class NumberConverter {
    int		integer;
    String	decimal;
    String	binary;
    String 	hex;
    String value;
    String[] components;


    //A function which takes the initial input and converts it to an int value
    public int ConvertToInt(String value) {
        components = value.split(":");
        if (components[0].equalsIgnoreCase("d"))
            integer = Integer.parseInt(components[1]);
        else if (components[0].equalsIgnoreCase("b"))
            integer = Integer.parseInt(components[1], 2);
        else if (components[0].equalsIgnoreCase("h"))
            integer = Integer.parseInt(components[1], 16);
        return integer;

    }

    //A function which converts an int value to a decimal value
    public String ConvertToDecimal(int integer) {
        decimal = Integer.toString(integer);
        return "d:" + decimal;
    }

    //A function which converts an int value to a binary number
    public String ConvertToBinary(int integer) {
        binary = Integer.toBinaryString(integer);
        return "b:" + binary;
    }

    //A function which converts an int value to a hex number
    public String ConvertToHex(int integer) {
        hex = Integer.toHexString(integer);
        return "h:" + hex;
    }

}
