package ahmedabdelghani.assignment3;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText text = (EditText) findViewById(R.id.textField);
        System.out.println(text);
        final NumberConverter converter = new NumberConverter();


        //creating the three buttons and the text field
        final Button Dec = (Button) findViewById(R.id.dec);
        final Button Bin = (Button) findViewById(R.id.bin);
        final Button Hex = (Button) findViewById(R.id.hex);
        Dec.setBackgroundResource(R.drawable.normal);
        Bin.setBackgroundResource(R.drawable.normal);
        Hex.setBackgroundResource(R.drawable.normal);
        text.setBackgroundResource(R.drawable.text);


        //Setting the onClickListener for the decimal conversion button
        Dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Setting the colors and shapes of the three buttons
                Dec.setBackgroundResource(R.drawable.pressed);
                Bin.setBackgroundResource(R.drawable.normal);
                Hex.setBackgroundResource(R.drawable.normal);

                try {
                    int input = converter.ConvertToInt(text.getText().toString());
                    text.setText(converter.ConvertToDecimal(input));

                    //Error handling for invalid input that does not start with the proper format
                    if (!(converter.components[0].equalsIgnoreCase("b")) && !(converter.components[0].equalsIgnoreCase("h")) && !(converter.components[0].equalsIgnoreCase("d"))) {
                        throw new NumberFormatException();
                    } else {
                        text.setBackgroundResource(R.drawable.text);
                    }
                    //displaying the error message to the user
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException | NullPointerException err) {
                    text.setBackgroundResource(R.drawable.error);
                    text.setText("error");
                }
            }
        });

        //Setting the onClickListener for the binary conversion button
        Bin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Setting the colors and shapes of the three buttons
                Dec.setBackgroundResource(R.drawable.normal);
                Bin.setBackgroundResource(R.drawable.pressed);
                Hex.setBackgroundResource(R.drawable.normal);

                try {
                    int input = converter.ConvertToInt(text.getText().toString());
                    text.setText(converter.ConvertToBinary(input));
                    //Error handling for invalid input that does not start with the proper format
                    if (!(converter.components[0].equalsIgnoreCase("b")) && !(converter.components[0].equalsIgnoreCase("h")) && !(converter.components[0].equalsIgnoreCase("d"))) {
                        throw new NumberFormatException();
                    } else {
                        text.setBackgroundResource(R.drawable.text);
                    }
                    //displaying the error message to the user
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException | NullPointerException err) {
                    text.setBackgroundResource(R.drawable.error);
                    text.setText("error");
                }
            }
        });

        //Setting the onClickListener for the hexadecimal conversion button
        Hex.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Setting the colors and shapes of the three buttons
                Dec.setBackgroundResource(R.drawable.normal);
                Bin.setBackgroundResource(R.drawable.normal);
                Hex.setBackgroundResource(R.drawable.pressed);

                try {
                    int input = converter.ConvertToInt(text.getText().toString());
                    text.setText(converter.ConvertToHex(input));
                    //Error handling for invalid input that does not start with the proper format
                    if (!(converter.components[0].equalsIgnoreCase("b")) && !(converter.components[0].equalsIgnoreCase("h")) && !(converter.components[0].equalsIgnoreCase("d"))) {
                        throw new NumberFormatException();
                    } else {
                        text.setBackgroundResource(R.drawable.text);
                    }
                    //displaying the error message to the user
                } catch (ArrayIndexOutOfBoundsException | NumberFormatException | NullPointerException err) {
                    text.setBackgroundResource(R.drawable.error);
                    text.setText("error");
                }
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
