# Description
The app is designed to convert values between between decimals, binary and hexadecimal.

# Operation
When the app starts, you will automatically be prompted to enter a decimal value. However, you can choose to start with binary or hex. Simply click on the value you would like to start with and clear the text field. Then enter the value starting with the first letter of the value name followed by a collen and the value you wish to convert. Once you entered the value properly, select any of the other value buttons to see the converted equivelant of the entered value. If a value is entered incorrectly, "error" will appear in the text field. Do not include spaces in your input.

Below are examples of entered values as hexadecimal, binary and decimal respectively,

- H:f
- B:10110
- D:16